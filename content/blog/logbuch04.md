+++
date = "2019-06-05T18:14:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch #04"
description = "Neues aus dem Vereinsleben, Unterstützung, Stammtisch uvm."
categories = ["Allgemeines", "Verein", "Dienste"]
tags = ["Vereinsangelegenheiten", "Website", "Verein"]

+++
Hallo zusammen,

zur Halbzeit des Jahres gibt es wieder einiges zu berichten.
Ohne Großes drum und dran, fangen wir gleich an! ;)

----
###### Finanzen & Transparenz

Wie bereits im letzten Logbuch angekündigt, möchten wir nun regelmäßig unsere Einnahmen und Ausgaben Transparent auf der Webseite darstellen.
Dafür wurde die neue Unterseite [Unterstützung](/unterstuetzung/) geschaffen, die neben dem Finanziellen, auch Informationen zu den Themen Mitgliedschaft und Spenden beinhaltet. Bisher haben wir diese Möglichkeiten etwas versteckt, auf der "Über Uns" Seite, zur Sprache gebracht.

Auf Vorschlag eines Mitglieds haben wir außerdem auch unsere [FAQ](/faq/) aktualisiert. Sollten noch Fragen offen bleiben, beantworten wir sie natürlich gerne. ;)

---
###### Personelle Entwicklung

Derzeit sind für den Verein vierzehn Personen als Ehrenamtliche Mitarbeiter (Ehrenamtler) tätig, vier
davon entfallen auf den Vorstand. Die meisten Personen sind im Bereich Redaktion beschäftigt. Im
Monat Mai haben wir zwei Ehrenamtler dazugewinnen können: Einen zweiten Webentwickler und eine
Lektorin für die Redaktion.
Allgemein verzeichnen wir guten Zulauf über die letzten Monate hinweg.

Dennoch sind wir weiter auf der Suche nach Mitstreitern.
Im Moment suchen wir, ins-besonders, im Bereich der Technik und des Marketings. Einen Überblick, über die Möglichkeiten mitzumachen, kann man sich [hier](/mitmachen/) verschaffen.

---
###### Innovationsbüro, Chancen Hackathon

Von 14.6 bis 15.6.19, wird unser 1. Vorsitzender, gemeinsam mit Vertretern des Deutschen
Bundesjugendrings und der XSF Foundation, an dem Chancen-Hackathon des Bundesministerium für
Familie, Senioren, Frauen und Jugend teilnehmen. Bei dieser zweitägigen Veranstaltung geht es,
unter anderem, um die Frage wie die Gesellschaft im ins-gesamten von der Digitalisierung profitieren
kann. Wir möchten vor Ort, gemeinsam mit unseren Kooperationspartnern, "Lobbyarbeit" für das Thema
Digitale Kommunikation und den Datenschutz betreiben.

---
###### DBJR – Deutscher Bundesjugendring

Am 26.6.19, werden wir an dem ersten Workshop, der Reihe „Digitale Jugendarbeit“, des DBJR
teilnehmen. In diesem Workshop geht es um die Vernetzung von Expert*innen, die Förderung des
Austauschs quer durch verschiedene Fachbereiche und natürlich um die Frage,
wie man konkret die Herausforderungen der Digitalen Jugendarbeit bewältigen kann.
Auch hier möchten wir unsere Themenbereiche, bei den anwesenden Verantwortlichen stärker
positionieren, und mit unserer entsprechenden Expertise zur Verfügung stehen.

Ein entsprechendes Resümee, zu den beiden Veranstaltungen, werden wir dann im nächsten Logbuch oder einem gesonderten Blogbeitrag ziehen. :)

---
###### Webchat XMPP

Seit kurzem bieten wir auch einen [XMPP Webchat](https://webchat.anoxinon.me) an. Dieser kann derzeit nur mit einem Konto von Anoxinon.me genutzt werden und dient als Ergänzung zum unserem bestehenden XMPP Angebot. In der aktuellen Version funktioniert leider durch einen bereits bekannten [Bug](https://github.com/conversejs/converse.js/issues/1524) die OMEMO Verschlüsselung nicht richtig. Demzufolge ist darüber nur eine unverschlüsselte Kommunikation möglich. Sobald für dieses Problem ein Update bereit steht werden wir euch natürlich auf Mastodon darüber informieren.


---
###### Festplattenausfall

Leider gab es vor einiger Zeit einen Hardwaredefekt bei einem unserer Server. Da wir jedoch alle
Systeme im RAID Modus betreiben, gab es keine Einschränkung im laufenden Betrieb. Hier hat sich das technisches Konzept also bewährt. Mittlerweile ist die neue Festplatte bereits verbaut und in Betrieb.

---
###### Mumble Stammtisch

Zum Schluss noch einen Hinweis auf unseren Mumble Stammtisch. Dieser findet,
regelmäßig Sonntags, ab 20 Uhr, auf unserem [Mumble Server](/dienste/mumble/) statt. Wir reden dort über verschiedene Themen, auch abseits des
Vereins. Dazukommen kann jeder der Lust hat, ob Vereinsmitglied oder nicht. :)

---

Soweit alles Wichtige.   
Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Viele Grüße,

euer Anoxinon Team
