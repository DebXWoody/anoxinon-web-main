--- 
date: "2021-07-14T22:00:00+01:00" 
draft: false 
author: "Anoxinon" 
title: "XMPP Newsletter Juni 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Juni 2021" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/06/newsletter-06-june). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat Juni 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:
- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Interessiert daran, dass Newsletter-Team zu unterstützen? Ließ mehr am Ende! Ansonsten - viel Spaß beim Lesen!

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news/jabberfr.org/category/newsletter/)
- Italienisch auf [NicFab.it](https://www.nicfab.it/la-newsletter-xmpp-di-marzo-2021-versione-italiana/)
Vielen Dank an all die Übersetzer:innen und deren Arbeit! Das ist eine große Hilfe dabei, die Neuigkeiten zu verbreiten! Bitte unerstützt sie oder steuert eine weitere Sprache bei!

## Ankündigungen

Der XMPP Operators Channel ist ein Ort, an dem vorrangig Administrator:innen von föderierten XMPP-Diensten einen gemeinschaftlichen und unanspruchsvollen Diskurs haben und Interoperabilitätsfehler lösen können. Während Diskussionen auch vom Thema abschweifen drüfen, gab es dennoch bisher keinen Code of Conduct oder andere Regeln bis diesen Monat. Nun kannst du sie [hier [EN]](https://xmpp.org/community/channels/operators) nachlesen und beitreten, wenn dich das Themengebiet interessiert.

## Veranstaltungen

Die wöchentlichen [XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Dazu kannst du gleich auf unserem neuen [YouTube Channel [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) vorbeischauen!

[Berlin XMPP Meeting (virtuell) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Das monatliche Treffen der XMPP-Enthusiast:innen aus Berlin - jeden zweiten Mittwoch eines Monats.

## Videos

[Demo: Ad-Hoc Befehle und Datenformate in Mellium [EN]](https://www.youtube.com/watch?v=C2oyAfJeqno) - Sam Whited demonstriert die neu hinzugekommene Implementation für [XEP-0050: Ad-Hoc Commands [EN]](https://xmpp.org/extensions/xep-0050.html) und [XEP-0004: Data Forms [EN]](https://xmpp.org/extensions/xep-0004.html) in der Mellium XMPP-Bibliothek.

## Artikel

Axel Reimer hat eine kurze Anleitung für die Installation und Konfiguration von Monal 5.0 veröffentlicht. Es ist in [Deutsch](https://eversten.net/blog/monal/) und [Englisch](https://eversten.net/en/blog/monal/) zu lesen.

Ingrid’s Space hat einige Gedanken dazu, [warum dezentralisierte Apps nicht funktionieren [EN]](https://ingrids.space/posts/why-distributed-systems-dont-work/) [würden], niedergeschrieben.

Martin Dosch hat einen kurzen Blogbeitrag veröffentlicht, in dem beschrieben wird, [wie man den "Jabber-ID" E-Mail-Header mit Neomutt verwendet [DE]](https://blog.mdosch.de/2021/06/13/jabber-email-header/). Das Ganze ist auch nochmal im [xmpp.org Wiki [EN]](https://wiki.xmpp.org/web/Jabber_Email_Header) beschrieben.

Die Software freedom conservancy [hat ihren Chat von IRC zu XMPP umgezogen [EN]](https://sfconservancy.org/blog/2021/jun/21/xmpp-chat/), dafür verwenden sie nun [Snikket [EN]](https://snikket.org/).

Vaxbox, der XMPP-basierte Impfbot wurde [in den Kanadischen Nationalnachrichten erwähnt [EN]](https://monal.im/blog/vaxbot-on-canadian-national-news/).

## Software News

### Clients und Apps

Der Blabber.IM XMPP-Dienst wurde eingestellt und die Server abgeschaltet. Am 25. Juni wurde auf der [blabber.im Webseite [EN]](https://blabber.im/) bekanntgegeben, dass sie den Dienst aus persönlichen Gründen einstellen.

Bezüglich dessen hat [MattJ ein Tool vorgestellt [EN]](https://mastodon.technology/@mattj/106517206134989817), mit dem Nutzer:innen ihre Kontaktliste / ihr Roster, sowie ihre vCards und Abbonements zu einem anderen Konto transferieren können. [Der XMPP-Kontoexporter ist hier zu finden [EN]](https://migrate.modernxmpp.org/).

[Gajim News aus der Entwicklung [EN]](https://gajim.org/de/post/2021-06-27-development-news-june/): Diesen Monat wurden Fehler in der Benutzeroberfäche und bei der Rechtschreibkorrektur behoben. Die Entwicklung von Gajim hat einen großen Schritt vorwärts gemacht: Dateivorschauen funktionieren wieder und noch wichtiger als das, sind sie nun direkt in Gajim implementiert [Anmerkung: Statt als Plugin], sehen noch besser aus und repektieren die Privatsphäre für diejenigen, die anonym chatten möchten.

Monal 5.0 [wurde veröffentlicht [EN]](https://monal.im/blog/monal-with-group-chat-and-voice-messages/), damit kam die Unterstützung für Gruppenchats (bisher ohne OMEMO, das kommt als nächstes) und Sprachnachrichten hinzu. Genauso gab es viele Veränderungen am internen Quelltext und einige Fehlerbehebungen. 

![Screenshot von Monal 5.0, es zeigt den offiziellen Monal-Gruppenchat](/img/blog/xmpp_newsletter_juni_2021/MonalGroupChat.png)
Profanity, eine konsolenbasierte XMPP-Anwendung hat [attention flags [EN]](https://profanity-im.github.io/blog/post/attention-attention/) implementiert. Mit der Tastenkombination `ALT + F` kann der Marker gesetzt werden.

UWPX [v0.33.0.0 [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.33.0.0) wurde veröffentlicht. Damit kommt die Unterstützung für [XMPP Provider [EN]](https://invent.kde.org/melvo/xmpp-providers/-/tree/master) hinzu, um eine angenehme Autovervollständigung und Registration für neue Konten auf XMPP-Servern zu ermöglichen. Darüberhinaus behob dieses Release einige Fehler bezogen auf das  "stream close" und das Parsing von offenen Nachrichten.

![Screenshot von UWPX, mit den Servervorschlägen der XMPP-Provider-Liste](/img/blog/xmpp_newsletter_juni_2021/UWPX.png)

### Server

[Jackal [EN]](https://github.com/ortuman/jackal), ein in Go geschriebener XMPP-Server, hat [Version 0.54.0 [EN]](https://github.com/ortuman/jackal/releases/tag/v0.54.0) mit Unterstützung für [XEP-0198 [EN]](https://xmpp.org/extensions/xep-0198.html) veröffentlicht: Stream Management, Unterstützung für Docker Compose, Graphana Service Dashboard und ein neues schickes Logo:
![Logo Jackal](/img/blog/xmpp_newsletter_juni_2021/jackal.png)

[OpenFire 4.6.4 [EN]](https://discourse.igniterealtime.org/t/openfire-4-6-4-is-released/90359) wurde veröffentlicht und bringt einige Fehlerkorrekturen.

### Libraries (Bibliotheken)

Hast du schon von qxbridge gehört, einer [Qt XMPP Bridge für Telegram [EN]](https://invent.kde.org/lnj/qxbridge/) basierend auf QXmpp.

Es gibt ein neues [ProtoXEP für die Pre-Auth-Schlüsselgenerierung [EN]](https://xmpp.org/extensions/inbox/preauth-ibr.html), was ausgefallene neue Anwendungen ermöglichen würde, wie beispielsweise [XMPP Self Provisioning [EN]](https://github.com/mellium/fediverse-xmpp-onboarding) mit Mastodon.

## Extensions und Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welche die formalen und anerkannten Definitionen für die Types (Typen), States (Status) and Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

* [Pre-auth-Registrierungsschlüssel-Generierung und -Validierung [EN]](https://xmpp.org/extensions/inbox/preauth-ibr.html)
  * Diese Spezifikation aktualisiert XEP-0401 und XEP-0445, indem sie ein gemeinsames Format für das vorauthentifizierte Registrierungs-Token spezifiziert.
* [Moved 2.0 [EN]](https://xmpp.org/extensions/inbox/moved2.html) 
  * Diese Spezifikation beschreibt eine Möglichkeit für Benutzer:innen, die eigenen Kontakte über einen Kontowechsel zu benachrichtigen.

### New (Neu)

* Version 0.1.0 von [XEP-0459 [EN]](https://xmpp.org/extensions/xep-0459.html) (XMPP Compliance Suites 2022)
  * Angenommen durch Abstimmung des Rates am 26.05.2021.
* Version 0.1.0 von [XEP-0458 [EN]](https://xmpp.org/extensions/xep-0458.html) (Community Code of Conduct)
  * Akzeptiert als Experimentell nach einstimmiger Annahme des ProtoXEP-Entwurfs durch den Rat zur Diskussion innerhalb der Community.

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

* Diesen Monat wurde kein XEP zurückgestellt.

### Aktualisiert

* Version 0.2.0 von [XEP-0458 [EN]](https://xmpp.org/extensions/xep-0458.html) (Verhaltenskodex der Gemeinschaft)
  * Verschiedene Kommentare aus verschiedenen Quellen einbinden (dwd)
* Version 0.3 von [XEP-0377 [EN]](https://xmpp.org/extensions/xep-0377.html) (Spam-Meldungen)
  * Überarbeitung basierend auf dem Feedback der Liste. (ssw)
* Version 0.11 von [XEP-0292 [EN]](https://xmpp.org/extensions/xep-0292.html) (vCard4 über XMPP)
  * Verwendung von kontaktlosen JIDs für Item-IDs empfehlen (ka)
* Version 1.20.0 von [XEP-0060 [EN]](https://xmpp.org/extensions/xep-0060.html) (Publish-Subscribe)
  * Integer-or-max-Datentyp zur Verwendung mit Data Forms Validation hinzufügen. (pep)

### Obsolet

* Version 1.0.0 von [XEP-0423 [EN]](https://xmpp.org/extensions/xep-0423.html) (XMPP Compliance Suites 2020)
  * Update auf Draft gemäß Ratsabstimmung am 07.11.2019
  * Nachfolger sind die XMPP Compliance Suites 2021 ([XEP-0443 [EN]](https://xmpp.org/extensions/xep-0443.html)) und 2022 ([XEP-0459 [EN]](https://xmpp.org/extensions/xep-0459.html))

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Call gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für den Aufstieg zum Entwurf zurück an den Council geht.

* Kein Last Call diesen Monat.

### Draft (Entwutf)

* Kein Entwurf in diesem Monat.

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist er jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

* Kein Call for Experience in diesem Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an Adrien Bourmault, alkino, BAud, emus, Jeybe, nicola, mdosch, Leirda, Licaon_Kter, Pierre Jarillon, pmaziere, seveso, VI, wurstsalat, xdelatour, Ysabeau für ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Twitter](https://twitter.com/xmpp)
* [Mastodon](https://fosstodon.org/@xmpp/)
* [YouTube](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook](https://www.facebook.com/jabber/)
* [Reddit](https://www.reddit.com/r/xmpp/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

