--- 
date: "2021-06-10T19:30:00+01:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter Mai 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Mai 2021" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/05/newsletter-05-may/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat Mai 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. 
Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:
- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Interessiert daran, dass Newsletter-Team zu unterstützen? Ließ mehr am Ende! 

Ansonsten - viel Spaß beim Lesen!

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [jabberfr.org](https://news/jabberfr.org/category/newsletter/)
- Italienisch auf [NicFab.it](https://www.nicfab.it/la-newsletter-xmpp-di-marzo-2021-versione-italiana/)

## XSF Ankündigungen

Über die Bewerbungen von Mitgliedern kann via [memberbot@xmpp.org](xmpp:memberbot@xmpp.org) abgestimmt werden (ausschließlich von XSF-Mitgliedern). 
Wir werden am 10. Juni eine Mitgliederversammlung abhalten, um die Ergebnisse dort formal zu bestätigen. Die Einzelheiten sind:

* Datum: 10. Juni 2021
* Zeit: 19:00 Uhr UTC
* Ort: [xsf@muc.xmpp.org](xmpp:xsf@muc.xmpp.org?join)
* Weiterführende Informationen: [https://wiki.xmpp.org/web/Membership_Applications_Q2_2021](https://wiki.xmpp.org/web/Membership_Applications_Q2_2021)

## Veranstaltungen

[Wöchentliche XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - schau' auch mal auf unserem neuen [YouTube-Kanal](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) vorbei!

[Berlin XMPP Meetup (virtuell) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Monatliches Treffen von XMPP-Enthusiasten in Berlin - immer am 2. Mittwoch eines jeden Monats. 
Das nächste Treffen wird am Mittwoch, dem 09.06.2021 sein und das Thema [gepflegte XMPP-Anbieter:innenliste [EN]](https://wiki.xmpp.org/web/Membership_Applications_Q2_2021) sein.

## Videos

[Gajim 1.4 UI/UX Vorschau [EN]](https://www.youtube.com/watch?v=SwZaZY2hYzA), vorgestellt von Philipp Hörist.

![Bildschirmfoto des Gajim-Hauptfensters](/img/blog/xmpp_newsletter_mai_2021/gajim_youtube.png)

## Artikel

JC Brand, der Entwickler hinter [Converse.js [EN]](https://conversejs.org/), dem Web-Client, hat über die aktuelle Entwicklung hin zu Version 8.0.0 in dem Artikel 
["Mergebounce: Increasing performance by batching IndexedDB writes" [EN]](https://opkode.com/blog/2021-05-05-mergebounce-indexeddb/) geschrieben.

Ingo Jürgensmann hat den Artikel ["The Fediverse – What About Resources?" [EN]](https://blog.windfluechter.net/2021/05/01/the-fediverse-what-about-ressources/) geschrieben, 
der die Ressourcen verschiedener Messaging-Technologien behandelt. Er betont, dass XMPP weitaus weniger Hardware-Ressourcen und damit Energie benötigt, als vergleichbare Dienste.

Sumit Khanna hat einen Artikel darüber geschrieben, [die eigenen Telefonnummern von Google Hangouts/Voice zu einem SIP/XMPP-Dienst zu verschieben [EN]](https://battlepenguin.com/tech/moving-my-phone-number-from-google-hangouts-voice-to-an-sip-xmpp-service/). 
Dafür wird XMPP und [jmp.chat [EN]](https://jmp.chat/) genutzt.

[jmp.chat [EN]](https://jmp.chat/) wird ebenfalls von [https://blog.craftyguy.net/ [EN]](https://blog.craftyguy.net/) verwendet, um Test-MMS 
bei der Entwicklung von [mmsd [EN]](https://gitlab.com/kop316/mmsd/) an ihn selbst zu senden.

[Vaxbot US wurde abgeschaltet [EN]](https://monal.im/blog/vaxbot-us-shutting-down/), nachdem sich die Situation in den USA verändert hat. Stattdessen wurde der Dienst nun 
für Kanada ausgerollt. Insgesamt war das ein interessanter Ansatz für die Verwendung von XMPP!

## Software News

### Clients und Apps

[Videoanrufe in Dino [EN]](https://fosstodon.org/web/statuses/106228549009869402) machen langsam Fortschritte. Die Dino-Entwickler:innen machen schon erfolgreich OMEMO-verschlüsselte Videoanrufe.
Die Funktion ist aktuell in den Nightly-Builds, dennoch gibt es noch weitere Arbeit, die erledigt werden muss.

![Bildschirmfoto des Dino-Videoanrufbildschirms](/img/blog/xmpp_newsletter_mai_2021/Dino_AVcalls.png)

[Gajim News aus der Entwicklung [DE]](https://gajim.org/de/post/2021-05-28-development-news-may/): Diesen Monat kamen verbesserte Ad-Hoc Kommandos, Fehlerbehebungen für Gajim Portable und
neue Bildvorschaufunktionen hinzu. In der Zwischenzeit macht die Arbeit an der nächsten Version von Gajim Fortschritte: Bessere Formatierung von Code-Blöcken, Chat-Filter, Notizen an sich
selbst und vieles mehr. Auch in den News: [Gajim hat diesen Monat seinen 17. Geburtstag gefeiert [DE]](https://gajim.org/de/post/2021-05-21-gajims-birthday/). Philipp Hörist (lovetox), der
Entwickler von Gajim, hat eine [Vorschau von Gajim 1.4 [EN]](https://www.youtube.com/watch?v=SwZaZY2hYzA) gezeigt. Gajim ist ein in Python geschriebener XMPP-Client. Aktuell wird er einer
großen UI-Überarbeitung unterzogen, wobei die ersten Ergebnisse in den [XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) vorgestellt wurden. 

[Kaidan 0.8 [EN]](https://www.kaidan.im/2021/05/28/kaidan-0.8.0/) wurde veröffentlicht und beinhaltet einige erwähnenswerte neue Funktionen, dazu zählen die Tippbenachrichtigungen 
([Chat State Notzifications [EN]](https://xmpp.org/extensions/xep-0085.html)) und die Nachrichtenverlaufssynchronisation (mittels [MAM [EN]](https://xmpp.org/extensions/xep-0313.html)).

"Salut à Toi" ist nun "Libervia". [Ließ mehr über die Veränderungen hinter dem Vorhang [EN]](https://www.goffi.org/b/libervia-progress-note-2021-w18-i4HD).

UWPX, ein Microsoft Windows (UWP) client, wurde in [Version 0.32.0.0 mit Fokus auf MUC und MAM Fehlerbehebungen [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.32.0.0) veröffentlicht.

### Server

ProcessOne hat einen Beitrag über die [Installation und Konfiguration von MariaDB mit ejabberd [EN]](https://www.process-one.net/blog/install-and-configure-mariadb-with-ejabberd/) 
veröffentlicht.

[Prosody 0.11.9 wurde veröffentlicht [EN]](https://blog.prosody.im/prosody-0.11.9-released/): Diese Veröffentlichung geht eine Nummer von wichtigen Sicherheitsproblemen an, die die meisten
produktiven Installationen von Prosody betreffen. Alle Details sind in einem [separaten Sicherheitsgutachten [EN]](https://prosody.im/security/advisory_20210512/) einzusehen. Wir
empfehlen, dass alle produktiven Instanzen aktualisieren oder die Maßnahmen aus dem Gutachten umsetzen.

Snikket [hat das Mai-Update [EN]](https://snikket.org/blog/may-2021-server-release/) für die Snikket Server Software herausgegeben. Dieses beinhaltet auch einige Sicherheitsverbesserungen
von Prosody, also aktualisiere schnellstmöglich! Außerdem ermöglicht es Nutzer:innenrollen und Zugriffslevel zu verwalten.

### Libraries (Bibliotheken)

Keine Aktualisierungen bei XMPP-Bibliotheken, die uns bekannt wären :-(

## Extensions und Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welche die formalen und anerkannten Definitionen für die Types (Typen), States (Status) and Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

* [XMPP Compliance Suites 2022 [EN]](https://xmpp.org/extensions/inbox/cs-2022.html)
	* Dieses Dokument definiert Programmkategorien von XMPP für verschiedene Anwendungszwecke (Core, IM, Web und Mobil) und spezifiziert die erforderlichen XEPs, die Clients und Server implementieren müssen, um mit diesen Anwedungszwecken kompatibel zu sein.

### New (Neu)

Keine neue XEP in diesem Monat.

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

Kein zurückgestelltes XEP diesen Monat.

### Aktualisiert

* Version 0.7.0 von [XEP-0373 [EN]](https://xmpp.org/extensions/xep-0373.html) (OpenPGP for XMPP)
	* PubSub access model 'open' für Public Key data nodes und metadata nodes vorschlagen
* Version 1.3 von [XEP-0013 [EN]](https://xmpp.org/extensions/xep-0013.html) (Flexible Offline Message Retrival)
	* Nach der Abstimmung des Councils vom 31.03.2021 ablehnen / als veraltet ansehen (XEP Editor:in (jsc))

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Call gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für den Aufstieg zum Entwurf zurück an den Council geht.

Keine Last Calls diesen Monat.

### Draft (Entwutf)

Keine Entwürfe diesen Monat.

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

Keine Calls for Experience diesen Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an emus, Florent Zara, Goffi, jeybe, Licaon_Kter, mdosch, nicola, pmaziere, snark, wurstsalat and Ysabeau ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Twitter](https://twitter.com/xmpp)
* [Mastodon](https://fosstodon.org/@xmpp/)
* [YouTube](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook](https://www.facebook.com/jabber/)
* [Reddit](https://www.reddit.com/r/xmpp/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

