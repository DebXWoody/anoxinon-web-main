--- 
date: "2021-08-09T23:59:59+01:00" 
draft: false
author: "Anoxinon" 
title: "XMPP Newsletter Juli 2021" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den Juli 2021" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/07/newsletter-07-july). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat Juli 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:
- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Interessiert daran, dass Newsletter-Team zu unterstützen? Ließ mehr am Ende! Ansonsten - viel Spaß beim Lesen!

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news/jabberfr.org/category/newsletter/)

## Ankündigungen

Derzeit stimmen die XSF-Mitglieder über neue Mitglieder und Wiederbewerber ab. Die Mitgliederversammlung findet am 19. August 2021, 19:00 UTC statt, um die Abstimmungsergebnisse formell zu bestätigen. XSF-Gruppenchat (MUC): Wenn du Interesse hast, der XSF beizutreten, kannst du dich ab dem 4. Quartal 2021 ebenfalls bewerben!

Seit diesem Monat ist auch eine neue Sub-Domain auf xmpp.net verfügbar: data.xmpp.net. Vielen Dank an MattJ! Das erste Datenprojekt, das hier für jeden zugänglich gehostet wird, sind die Providerlisten (JSON-Format) aus dem XMPP Providers Projekt. Es gibt bereits erste Client-Implementierungen, die diese verwenden - bitte prüfe die Kriterien und füge deinen Dienst ebenfalls über das Gitlab-Repository hinzu! Feedback ist willkommen!

## Veranstaltungen

Die wöchentlichen [XMPP Office Hours [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) - Dazu kannst du gleich auf unserem neuen [YouTube Channel [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA) vorbeischauen!

[Berlin XMPP Meeting (virtuell) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Das monatliche Treffen der XMPP-Enthusiast:innen aus Berlin - jeden zweiten Mittwoch eines Monats.

## Videos

XMPP-Sprechstunden: [Aufbau eines Chat-Bots auf Ad-hoc-Befehlen [EN Youtube]](https://www.youtube.com/watch?v=e1-hpsQ9OZE)

## Artikel

Mit dem allerersten Artikel möchten wir die Aufmerksamkeit auf ein allgemeines, ernstes Thema lenken: [Burnout in Open-Source-Gemeinschaften [EN]](https://opensource.com/article/21/7/burnout-open-source). Bitte passt auf euch auf, suche dir Hilfe und hab' auch ein Auge auf deine virtuellen Kollegen! Es mag befriedigender sein, an deinem aktuellen Standort nach Hilfe zu suchen, aber dies könnte ein Anfang sein: [Psychology Today - Wie man eine Therapie findet [EN]](https://www.thecut.com/article/how-to-find-a-therapist.html)

Der Blog des Debian XMPP-Teams [kündigte all die Goodies an [EN]](https://xmpp-team.pages.debian.net/blog/2021/07/xmpp-novelties-in-debian-11-bullseye.html), die das bald erscheinende Debian 11 bringen wird. Während diese für die Leser:innen des Newsletters vielleicht nicht "neu" sind, werden sie die Erfahrung für Benutzer von Debian Stable erheblich verbessern.

Seth Kenlon, von Red Hat, hat zwei Artikel über die XML-Auszeichnungssprache (eine ziemlich wichtige Sache in der XMPP-Welt ;) ) auf [opensource.com [EN]](https://opensource.com/) veröffentlicht. Er beginnt mit [What is XML? [EN]](https://opensource.com/article/21/7/what-xml) und schließt mit [Use XMLStarlet to parse XML in the Linux terminal [EN]](https://opensource.com/article/21/7/parse-xml-linux) an.

## Software News

### Clients und Apps

[Gajim News [EN]](https://gajim.org/post/2021-07-28-development-news-july/): Die Entwicklung der neuen Gajim-Version wurde im Juli fortgesetzt und brachte viele Korrekturen und Verbesserungen. Außerdem diesen Monat: WebSocket-Verbesserungen und eine neue python-nbxmpp-Version.

Profanity [0.11.0 [EN]](https://github.com/profanity-im/profanity/releases/tag/0.11.0) ist erschienen, womit sechs Monate lang an der Version 0.10.0 gefeilt wurde. Dazu gehören die Unterstützung von Message Archive Management (MAM) (noch experimentell), Unterstützung für das Ändern des Passworts, Fähigkeiten in Gruppenchats (MUC) wie Sprache und Registrierung, OMEMO-Vertrauensmodus, private Nachrichten (MUC-PM) in öffentlichen Kanälen, Spam-Berichterstattung, Erkennung von Serverkontakten und vieles mehr.

[Jan-Philipp Litza](https://github.com/jplitza) und [mortzu](https://github.com/mortzu) bauen eine XMPP-Feed-Integration für die offizielle deutsche Warn-App NINA: [Das Github-Repository findest du hier. [EN]](https://github.com/mortzu/nina_xmpp) Man kann den [Bot einfach hinzufügen und Koordinaten von Interesse registrieren [EN]](https://github.com/mortzu/nina_xmpp/blob/master/README.md). Möge er dich nie kontaktieren!

UWPX [v.0.34.0.0 [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.34.0.0) ist veröffentlicht worden. Diese Version konzentriert sich hauptsächlich auf Fehlerbehebungen für die erste Betaversion von UWPX mit einer voraussichtlichen Veröffentlichung am 01.09.2021 und auf die richtige Push-Unterstützung, auch wenn die App nicht läuft. Zu diesem Zweck hat [COM8[EN]](https://github.com/COM8) in den letzten Monaten an seinem C++-[Push-Server [EN]](https://github.com/UWPX/UWPX-Push-Server) gearbeitet, der nun endlich einsatzbereit ist. Darüber hinaus enthält diese Version auch Verbesserungen in XEP-0085 (Chat State Notifications) mit einer korrekten [Tippen-Anzeige [EN]](https://github.com/UWPX/UWPX-Client/releases/tag/v.0.34.0.0) und Statusmeldungen.

### Server

jabberd 21.07 wurde mit einer Fülle von Fehlerkorrekturen und Verbesserungen [veröffentlicht [EN]](https://www.process-one.net/blog/ejabberd-21-07/), also unbedingt das Changelog lesen, wenn du gemeinsame Gruppen und MySQL berwendest. Große Änderungen wurden in Bezug auf das Build-System implementiert, da ejabberd nun mit [rebar3 [EN]](https://rebar3.org/) und [Elixir Mix [EN]](https://elixir-lang.org/getting-started/mix-otp/introduction-to-mix.html) gebaut werden kann.

Für OpenFire wurde ein Update für [das Plugin 'inVerse' veröffentlicht [EN]](https://discourse.igniterealtime.org/t/inverse-plugin-for-openfire-reaches-7-0-6-2/90440), das den [Converse.js Web-Client [EN]](https://conversejs.org/) für seine Nutzer verfügbar macht.

### Libraries (Bibliotheken)

[python-nbxmpp 2.0.3 [EN]](https://gajim.org/post/2021-07-28-development-news-july/) ist veröffentlicht worden.

[Mellium Dev Communiqué [EN]](https://opencollective.com/mellium/updates/dev-communique-for-july-2021): Die Entwicklung ging diesen Monat zügig weiter und umfasste die übliche Auswahl an Fehlerbehebungen und Verbesserungen. Darüber hinaus wurden Carbons, MUC und Roster Versionierung implementiert!

Smack, eine Java XMPP-Client-Bibliothek, wurde in [Version 4.4.3 [EN]](https://discourse.igniterealtime.org/t/smack-4-4-3-released/90436) mit vielen Fehlerkorrekturen veröffentlicht.

## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welche die formalen und anerkannten Definitionen für die Types (Typen), States (Status) and Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

- [Disco Feature Attachment [EN]](https://xmpp.org/extensions/inbox/disco-feature-attachment.html)
    - Mit dieser Spezifikation kann angegeben werden, dass ein Feature für einen bestimmten Namespace implementiert ist.
- [Pubsub Caching Hints [EN]](https://xmpp.org/extensions/inbox/pubsub-caching-hints.html)
    - Diese Spezifikation bietet eine Möglichkeit, Caching-Informationen von einem Pubsub-Knoten zu erhalten.

### New (Neu)

* Kein neuer XEP in diesem Monat.

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

* [XEP-0328 [EN]](https://xmpp.org/extensions/xep-0328.html) (JID Preparation and Validation Service)
* [XEP-0333 [EN]](https://xmpp.org/extensions/xep-0333.html) (Chat Markers)
* [XEP-0357 [EN]](https://xmpp.org/extensions/xep-0357.html) (Push Notifications)
* [XEP-0380 [EN]](https://xmpp.org/extensions/xep-0380.html) (Explicit Message Encryption)
* [XEP-0392 [EN]](https://xmpp.org/extensions/xep-0392.html) (Consistent Color Generation)
* [XEP-0398 [EN]](https://xmpp.org/extensions/xep-0398.html) (User Avatar to vCard-Based Avatars Conversion)
* [XEP-0401 [EN]](https://xmpp.org/extensions/xep-0401.html) (Easy User Onboarding)
* [XEP-0413 [EN]](https://xmpp.org/extensions/xep-0413.html) (Order-By)
* [XEP-0414 [EN]](https://xmpp.org/extensions/xep-0414.html) (Cryptographic Hash Function Recommendations for XMPP)
* [XEP-0415 [EN]](https://xmpp.org/extensions/xep-0415.html) (XMPP Over RELOAD (XOR))
* [XEP-0416 [EN]](https://xmpp.org/extensions/xep-0416.html) (E2E Authentication in XMPP)
* [XEP-0417 [EN]](https://xmpp.org/extensions/xep-0417.html) (E2E Authentication in XMPP: Certificate Issuance and Revocation)
* [XEP-0418 [EN]](https://xmpp.org/extensions/xep-0418.html) (DNS Queries over XMPP (DoX))
* [XEP-0421 [EN]](https://xmpp.org/extensions/xep-0421.html) (Anonymous unique occupant identifiers for MUCs)
* [XEP-0422 [EN]](https://xmpp.org/extensions/xep-0422.html) (Message Fastening)
* [XEP-0424 [EN]](https://xmpp.org/extensions/xep-0424.html) (Message Retraction)
* [XEP-0425 [EN]](https://xmpp.org/extensions/xep-0425.html) (Message Moderation)
* [XEP-0426 [EN]](https://xmpp.org/extensions/xep-0426.html) (Character counting in message bodies)
* [XEP-0427 [EN]](https://xmpp.org/extensions/xep-0427.html) (MAM Fastening Collation)
* [XEP-0428 [EN]](https://xmpp.org/extensions/xep-0428.html) (Fallback Indication)
* [XEP-0430 [EN]](https://xmpp.org/extensions/xep-0430.html) (Inbox)
* [XEP-0431 [EN]](https://xmpp.org/extensions/xep-0431.html) (Full Text Search in MAM)
* [XEP-0432 [EN]](https://xmpp.org/extensions/xep-0432.html) (Simple JSON Messaging)
* [XEP-0433 [EN]](https://xmpp.org/extensions/xep-0433.html) (Extended Channel Search)
* [XEP-0435 [EN]](https://xmpp.org/extensions/xep-0435.html) (Reminders)
* [XEP-0436 [EN]](https://xmpp.org/extensions/xep-0436.html) (MUC presence versioning)
* [XEP-0437 [EN]](https://xmpp.org/extensions/xep-0437.html) (Room Activity Indicators)
* [XEP-0439 [EN]](https://xmpp.org/extensions/xep-0439.html) (Quick Response)

### Updated (Aktualisiert)

- Version 1.0.0 von [XEP-0429 [EN]](https://xmpp.org/extensions/xep-0429.html) (Special Interests Group End to End Encryption)
    - Angenommen vom Rat (XEP-Editor: jsc)
- Version 0.2 von [XEP-0413 [EN]](https://xmpp.org/extensions/xep-0413.html) (Order-By)
    - Hinzufügen einer Möglichkeit um herauszufinden, auf welche Protokolle Order-By anwendbar ist.
    - Entfernen von Verweisen auf SQL (außer in Implementierungshinweisen).
    - Festlegen, dass Order-By auf die gesamte Objektmenge und innerhalb einer RSM-Ergebnismenge wirkt.
    - Explizite Angabe, dass das Erstellungs- und Änderungsdatum vom Pubsub-Dienst selbst festgelegt wird.
    - Festlegen, dass die Clark-Notation für Erweiterungen verwendet werden soll.
    - Hinzufügen eines vollständigen Beispiels mit Pubsub und RSM.
    - Hinweis für SQL-basierte Implementierungen hinzufügen [XEP-0060 [EN]](https://xmpp.org/extensions/xep-0060.html) und [XEP-0313 [EN]](https://xmpp.org/extensions/xep-0313.html) als Abhängigkeiten entfernen, sie werden als Anwendungsfälle erwähnt, sind aber nicht zwingend erforderlich.
    - Bessere Formulierung nach Feedback.
    - Namespace bump. (jp)
- Version 1.0.0 von [XEP-0381 [EN]](https://xmpp.org/extensions/xep-0381.html) (Internet of Things Special Interest Group (IoT SIG))
    - Angenommen vom Rat. (XEP Editor: jsc)
- Version 0.2.0 von [XEP-0383 [EN]](https://xmpp.org/extensions/xep-0383.html) (Burner JIDs)
    - Verbesserung der Sicherheitsaspekte und Hinzufügen von JIDs. (ssw)
- Version 0.2.0 von [XEP-0458 [EN]](https://xmpp.org/extensions/xep-0458.html) (Community Code of Conduct)
    - Integration verschiedener Kommentare aus unterschiedlichen Quellen. (dwd)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Call gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für den Aufstieg zum Entwurf zurück an den Council geht.

* Kein Last Call diesen Monat.

### Draft (Entwutf)

* Kein Draft diesen Monat.

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

* Kein Call for Experience diesen Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an Adrien Bourmault, Benoît Sibaud, DebXwoody, COM8, emus, mattJ, neox, Licaon_Kter, pmaziere, raspbeguy, wurstsalat3000 und Ysabeau ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Twitter](https://twitter.com/xmpp)
* [Mastodon](https://fosstodon.org/@xmpp/)
* [YouTube](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook](https://www.facebook.com/jabber/)
* [Reddit](https://www.reddit.com/r/xmpp/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

