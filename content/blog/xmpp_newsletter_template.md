--- 
date: "2050-01-01T23:59:59+01:00" 
draft: true
author: "Anoxinon" 
title: "XMPP Newsletter Template" 
description: "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den MONAT JAHR" 
categories:
- "Community"
- "Inhalte"
- "Open Source" 
tags:
- "XMPP"
- "Messenger"
- "XSF"
- "XMPP Newsletter"
---

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/202x/xx/newsletter-xx-MONTH/). Übersetzung und Korrektur der dt. Version von Anoxinon e.V..*

Willkommen zum XMPP-Newsletter für den Monat MONAT JAHR.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:

- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/category/newsletter.html)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news.jabberfr.org/category/newsletter/)
- Die italienische Version des letzten Monats auf [NicFab.it](https://www.nicfab.it/la-newsletter-xmpp-di-marzo-2021-versione-italiana/)

## Ankündigungen

## Veranstaltungen

## Videos

## Artikel

## Software News

### Clients und Apps

### Server

### Libraries (Bibliotheken)

## Extensions and Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welches die formalen und anerkannten Definitionen für die Types (Typen), States (Status) und Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

### New (Neu)

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

### Updated (Aktualisiert)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Calls gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für die Aufwertung zum Entwurf zurück an den Council geht.

### Stable (Stabil)
Info: Die XSF hat beschlossen, "Draft" in "Stable" umzubenennen. [Lesen Sie mehr darüber hier [EN]](https://github.com/xsf/xeps/pull/1100).

### Deprecated (Veraltet)

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an CREDITS ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Mastodon [EN]](https://fosstodon.org/@xmpp/)
* [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [Twitter [EN]](https://twitter.com/xmpp)
* [Reddit [EN]](https://www.reddit.com/r/xmpp/)
* [LinkedIn [EN]](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook [EN]](https://www.facebook.com/jabber/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neuigkeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die monatliche Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.

