+++ 
date = "2021-04-11T19:30:00+01:00" 
draft = false
author = "Jeybe" 
title = "XMPP Newsletter März 2021" 
description = "Die deutsche Übersetzung des XMPP-Newsletters der XSF für den März 2021" 
categories = ["Community", "Inhalte", "Open Source"] 
tags = ["XMPP", "Messenger", "XSF", "XMPP Newsletter"]
+++

*Info: [Anoxinon e.V.](https://anoxinon.de/) publiziert die deutsche Übersetzung des unter der CC by-sa 4.0 Lizenz stehenden XMPP Newsletters für die XSF Foundation. Den [Originalartikel findest Du im Blog der XSF](https://xmpp.org/2021/03/newsletter-03-march/).  Übersetzung und Korrektur der dt. Version von Jeybe.*

Willkommen zum XMPP-Newsletter für den Monat März 2021.

Viele Projekte und ihre Bemühungen in der XMPP-Gemeinschaft sind das Ergebnis der ehrenamtlichen Arbeit von Freiwilligen. Wenn Du mit den Diensten und der Software, die Du vielleicht nutzt, zufrieden bist, vor allem im letzten Jahr, denk' bitte daran, Dich zu bedanken oder diesen Projekten zu helfen!

Ließ diesen Newsletter als RSS-Feed:
- [Das englische Original](https://xmpp.org/feeds/all.atom.xml)
- [Die deutsche Übersetzung bei Anoxinon e.V.](https://anoxinon.de/tags/xmpp-newsletter/index.xml)

Interessiert daran, dass Newsletter-Team zu unterstützen? Ließ mehr am Ende! Ansonsten - viel Spaß beim Lessen!

### Übersetzungen des Newsletters

Die Übersetzungen des Newsletters werden auf folgenden Seiten veröffentlicht:

- Englisch (Original) auf [xmpp.org](https://xmpp.org/2021/01/newsletter-01-january/)
- Französisch auf [linuxfr.org](https://linuxfr.org/tags/xmpp/public) und [jabberfr.org](https://news/jabberfr.org/category/newsletter/)
- Die Deutsche Übersetzung [von letztem Monat](https://anoxinon.de/blog/xmpp_newsletter_februar_2021/)

## Ankündigungen

Die XMPP-Gemeinschaft hält von nun an regelmäßige "Arbeitsstunden", kurze wöchentliche Talks oder Diskussionen über XMPP oder XMPP-relevante Themen ab.
Details über die Videokonferenzen, eine Liste von kommenden Talks und das Anmeldeformular kann im [XMPP-Wiki [EN]](https://wiki.xmpp.org/web/XMPP_Office_Hours) gefunden werden. Außerdem sind wir nun auf [YouTube [EN]](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)!

## Veranstaltungen

[Wöchentliche XMPP Arbeitsstunden [EN]!](https://wiki.xmpp.org/web/XMPP_Office_Hours)

[Berlin XMPP Treffen (virtuell) [EN]](https://mov.im/?node/pubsub.movim.eu/berlin-xmpp-meetup): Monatliches Treffen von XMPP-Enthusiasten in Berlin, jeden zweiten Mittwoch eines jeden Monats.

## Artikel

Nicola Fabiano hat zwei Artikel zum Thema [Bewusster Blick auf Messaging-Apps: Wenn Nutzer:innen weitgehende Freiheit und volle Kontrolle über ihre Daten wollen [EN]](https://www.nicfab.it/consciously-looking-at-messaging-apps-when-users-want-broad-freedom-and-full-control-over-their-data/) und [Bewusste digitale Kommunikation unter Beachtung der Privatsphäre und der von dir gewählten Apps oder Dienste [EN]](https://www.nicfab.it/aware-digital-communication-respecting-privacy-and-the-apps-or-services-you-choose/) geschrieben.

Arnaud Joset hat eine kurze Anleitung geschrieben, [wie man Prosody mit einem HTTP Reverse Proxy verwenden kann [EN]](https://blog.agayon.be/prosody_http.html).

Marek Foss von ProcessOne hat einige Artikel über ejabberd Background und Setups geschrieben:

- [Einführung in das MQTT-Protokoll und den ejabberd MQTT Broker [EN]](https://www.process-one.net/blog/starting-with-mqtt-protocol-and-ejabberd-mqtt-broker/)
- [MariaDB für ejabberd installieren und konfigurieren [EN]](https://www.process-one.net/blog/install-and-configure-mariadb-with-ejabberd/)
- [Einführung in die WebSockets API von ejabberd [EN]](https://www.process-one.net/blog/getting-started-with-websocket-api-in-ejabberd/)
- [Publish-Subscribe Pattern und PubSub in ejabberd verwenden [EN]](https://www.process-one.net/blog/publish-subscribe-pattern-and-pubsub-in-ejabberd/)

Alex Akinbia und Ehizojie Ojieb haben einen Artikel zum Thema [forensische Analyse von Open Source XMPP Apps auf iOS-Geräten [EN]](https://www.sciencedirect.com/science/article/pii/S2666281721000196?via%3Dihub) veröffentlicht. Die Studie basiert auf Monal 4.5 und Siskin 5.8.1.

## Software News

### Clients und Apps

[Conversation 2.9.8 wurde veröffentlicht [EN]](https://github.com/iNPUTmice/Conversations/releases/tag/2.9.8). Diese Version verbessert die Kompatibilität mit WebRTC-Implementierungen abseits von libwebrtc und verifiziert A/V-Anrufe mit schon existierenden OMEMO-Sitzungen.

![Conversations A/V Verifikation](/img/blog/xmpp_newsletter_märz_2021/conversations.png "Conversations A/V Verifikation")

[Gajim News aus der Entwicklung - März 2021 [DE]](https://gajim.org/de/post/2021-03-28-development-news-march/): Gajims neues Hauptfenster nimmt Schritt für Schritt Gestalt an, während im gleichen Zug viele der Kernfunktionalitäten umgeschrieben werden. Diesen Monat wurde auch die Barrierefreiheit für die Nachrichteneingabe sowie das Verhalten der Status-Symbole verbessert. [Gajim 1.3.1 ist erschienen [DE]](https://gajim.org/de/post/2021-03-01-gajim-1.3.1-released/), dabei kam neben Detailverbesserungen unter anderem eine Einstellung hinzu, um die GSSAPI Authentifizierung zu aktivieren.

JSXC - der Javascript XMPP Client hat eine [Finanzierung für Gruppenanrufe erhalten [EN]](https://twitter.com/jsxc_org/status/1366352995508748294)

[Monal ist nun auf Mastodon [EN]](https://fosstodon.org/@Monal)! Die [zweite Beta [EN]](https://monal.im/blog/monal-5-beta-2-out/) wurde veröffentlicht und lässt Monal 5.0 wieder ein Stück näher kommen. Außerdem wurde ein Setup erstellt, [um den Impfprozess in vielen US-Staaten und Puerto Rico zu unterstützen [EN]](https://vaccine.monal.im/). Inzwischen wurden schon mehr als eine Millionen Benachrichtigungen verschickt. Zu diesem Thema gab es [diesen Monat einige Blogbeiträge [EN]](https://monal.im/2021/03/), Du kannst mit [diesem ausgewählten](https://monal.im/blog/xmpp-ending-this-pandemic-part-1/) beginnen. Bleib' auf dem Laufenden und hilf' durch das Testen der aktuellen Beta!

qXMPPconsole - Eine [browserbasierte XMPP Konsole](https://github.com/voger/qXMPPconsole). Sie dient hautpsächlich zum Lernen des XMPP-Protkolls.

### Server

Keine Server-Aktualisierungen da draußen.

### Bibliotheken

[Smack 4.4.2 wurde veröffentlicht [EN]](https://discourse.igniterealtime.org/t/smack-4-4-2-released/89913) und beinhaltet hauptsächlich Fehlerbehebungen. 

## Extensions und Specifications (Erweiterungen und Spezifikationen)

Entwickler:innen und andere Standard-Expert:innen von aller Welt arbeiten zusammen an diesen Extensions, entwicklen neue Specifications für aufkommende Praktiken und verfeinern die bestehenden Wege, Dinge umzusetzen. Proposed (Vorgeschlagen) werden können sie von jeder und jedem, wobei die besonders erfolgreichen es in den Status Final oder Active (Aktiv) schaffen - je nach Typ. Andere hingegen werden bedacht als Deferred (Zurückgestellt) markiert. Dieser Lebenszyklus ist in [XEP-0001 [EN]](https://xmpp.org/extensions/xep-0001.html) beschrieben, welche die formalen und anerkannten Definitionen für die Types (Typen), States (Status) and Processes (Prozesse) enthält. [Ließ mehr über den Standards Process (Standard-Prozess) [EN]](https://xmpp.org/about/standards-process.html). Die Kommunikation rund um Standards und Extensions findet auf der [Standards Mailing List [EN]](https://mail.jabber.org/mailman/listinfo/standards) statt. ([Online Archiv [EN]](https://mail.jabber.org/pipermail/standards/).)

### Proposed (Vorgeschlagen)

Der XEP-Entwicklungs-Prozess startet damit, eine Idee auszuformulieren und sie an eine XMPP-Editorin oder einen XMPP-Editor zu übergeben. Innerhalb von zwei Wochen entscheidet der Council (Rat), ob der Vorschlag als Experimental XEP akzeptiert wird.

- [Content Rating Labels [EN]](https://xmpp.org/extensions/inbox/content-ratings.html) wurde vorgeschlagen und akzeptiert.

### New (Neu)

- Version 0.2.0 von [XEP-0456 (Content Rating Labels) [EN]](https://xmpp.org/extensions/xep-0456.html)
    - Diese Specification stellt ein Wire Format in Form einer Service Discovery Extension zur Verfügung, um es verschiedenen Diensten zu ermöglichen, verschiedene Informationen über die Art von Inhalten, die sie auf der Plattform erlauben und/oder dulden, zu veröffentlichen
    - Beschreibe den Conversion Algorithm (jc)

### Deferred (Zurückgestellt)

Wenn ein Experimental XEP für mehr als zwölf Monate nicht aktualisiert wurde, wird es von Experimental zu Deferred verschoben. Sobald es wieder eine Aktualisierung gibt, wird es wieder nach Experimental verschoben.

- Diesen Monat wurden keine XEPs Deferred

### Aktualisiert

- Version 1.1 von [XEP-0294 (Jingle RTP Header Extensions Negotiation) [EN]](https://xmpp.org/extensions/xep-0294.html)
- Version 0.2.0 von [XEP-0408 (Mediated Information eXchange (MIX): Co-existence with MUC) [EN]](https://xmpp.org/extensions/xep-0408.html)
    - Referent auf MIX-Core Namespaces korrigieren (@mathieui)
- Version 0.3.0 von [XEP-0406 (Mediated Information eXchange (MIX): MIX Administration) [EN]](https://xmpp.org/extensions/xep-0406.html)
    - Referent auf MIX-Core Namespaces korrigieren (@mathieui)
- Version 2.12.0 von [XEP-0004 (Data Forms) [EN]](https://xmpp.org/extensions/xep-0004.html)
    - Klarstellen, dass das 'reported'-Element vor jedem 'item'-Element stehen muss (fs, jsc)

### Last Call (Letzter Aufruf)

Last Calls werden ausgerufen, sobald jede und jeder mit dem aktuellen Status eines XEPs zufrieden ist. Nachdem der Council (Rat) entscheidet, ob ein XEP bereit scheint, ruft die XMPP-Editorin oder der XMPP-Editor einen Last Call for Comments (Letzten Aufruf für Kommentare) auf. Das Feedback, das während dieses Last Call gesammelt wird, hilft, das XEP nochmals zu verbessern, bevor es für den Aufstieg zum Entwurf zurück an den Council geht.

- [XEP-0280 [EN]](https://xmpp.org/extensions/xep-0280.html) - Message Carbons
- [XEP-0313 [EN]](https://xmpp.org/extensions/xep-0313.html) - Message Archive Management

### Draft (Entwutf)

Keine Drafts diesen Monat.

### Call for Experience (Aufruf für Erfahrungen)

Ein Call for Experience (ein Aufruf für Erfahrungen) ist, wie ein Last Call, eine explizite Aufforderung für Kommentare. In diesem Fall ist es jedoch hauptsächlich an die Menschen gerichtet, die die Specification implementiert und idealerweise auch ausgerollt haben. Dann stimmt der Council darüber ab, ob sie nach Final verschoben wird.

- Kein Call for Experience disen Monat.

## Danke an alle!

Dieser XMPP-Newsletter wurde kollaborativ von der Community erarbeitet.

Danke an anubis, Bastoon, emus, jeybe, jonas-l, Julien Jorge, Holger, peetah, Pierre Maziere, Sam Whited, seveso, vanitasvitae, wurstsalat3000 und Ysabeau für ihre Hilfe, ihn zu erstellen!

## Teile die Neuigkeiten!

Teile unsere Neuigkeiten in "Sozialen Netzwerken":

* [Twitter](https://twitter.com/xmpp)
* [Mastodon](https://fosstodon.org/@xmpp/)
* [YouTube](https://www.youtube.com/channel/UCf3Kq2ElJDFQhYDdjn18RuA)
* [LinkedIn](https://www.linkedin.com/company/xmpp-standards-foundation/)
* [Facebook](https://www.facebook.com/jabber/)
* [Reddit](https://www.reddit.com/r/xmpp/)

Finde und schreibe XMPP Job-Angebote im [XMPP job board (EN)](https://xmpp.work/).

[Abonniere (EN)](https://tinyletter.com/xmpp) diesen Newsletter und erhalte die nächste Edition direkt in Dein Postfach, sobald sie veröffentlicht wird.

Auch via RSS-Feed in [Englisch](https://xmpp.org/feeds/all.atom.xml) und in [Deutsch](https://anoxinon.de/tags/xmpp-newsletter/index.xml)!

## Hilf uns, diesen Newsletter zu erstellen

Wir haben den Entwurf dieses Newsletters in dieser [einfachen Notiz (EN)](https://yopad.eu/p/xmpp-newsletter-365days) parallel zu unseren Arbeiten im [XSF Github Repositorium (EN)](https://github.com/xsf/xmpp.org/milestone/3) begonnen. Wir freuen uns immer über Beteiligung. Zögere nicht, unserer Diskussion in unserem [Comm-Team Gruppenchat (EN)](xmpp:commteam@muc.xmpp.org?join) beizutreten und dabei zu helfen, den Newsletter in Community-Arbeit forzuführen.

Du hast ein Projekt und schreibst darüber? Denk’ doch darüber nach, die Neuigkeiten oder Veranstaltungen im Newsletter zu teilen und einer großen Zielgruppe zu präsentieren. Schon ein Beitrag von ein paar Minuten ist hilfreich!

Aufgaben, die regelmäßig erledigt werden müssen:

- Neugikeiten aus dem XMPP-Universum zusammenfassen
- Neuigkeiten und Veranstaltungen in Textform bringen
- Die Kommunikation über Erweiterungen (XEPs) zusammenfassen
- Den Entwurf des Newsletters gegenlesen
- Medienbilder vorbereiten
- Übersetzungen: Vor allem Deutsch und Spanisch

## Lizenz

Dieser Newsletter wird unter der [CC BY-SA Lizenz [DE]](https://creativecommons.org/licenses/by-sa/4.0/deed.de) veröffentlicht.
